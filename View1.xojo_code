#tag IOSView
Begin iosView View1
   BackButtonTitle =   ""
   Compatibility   =   ""
   LargeTitleMode  =   "2"
   Left            =   0
   NavigationBarVisible=   False
   TabIcon         =   ""
   TabTitle        =   ""
   Title           =   ""
   Top             =   0
   Begin iOSTextArea TextArea1
      AccessibilityHint=   ""
      AccessibilityLabel=   ""
      AutoLayout      =   TextArea1, 1, <Parent>, 1, False, +1.00, 4, 1, *kStdGapCtlToViewH, , True
      AutoLayout      =   TextArea1, 2, <Parent>, 2, False, +1.00, 4, 1, -*kStdGapCtlToViewH, , True
      AutoLayout      =   TextArea1, 4, Button2, 3, False, +1.00, 4, 1, -8, , True
      AutoLayout      =   TextArea1, 3, TopLayoutGuide, 4, False, +1.00, 4, 1, *kStdControlGapV, , True
      BorderColor     =   "&h00000000"
      BorderStyle     =   "0"
      Editable        =   True
      Height          =   368.0
      KeyboardType    =   "0"
      Left            =   20
      LockedInPosition=   False
      Scope           =   0
      Text            =   "<?xml version=""1.0"" encoding=""UTF-8""?>\n\n<bookstore>\n\n<book category=""cooking"">\n  <title lang=""en"">Everyday Italian</title>\n  <author>Giada De Laurentiis</author>\n  <year>2005</year>\n  <price>30.00</price>\n</book>\n\n<book category=""children"">\n  <title lang=""en"">Harry Potter</title>\n  <author>J K. Rowling</author>\n  <year>2005</year>\n  <price>29.99</price>\n</book>\n\n<book category=""web"">\n  <title lang=""en"">XQuery Kick Start</title>\n  <author>James McGovern</author>\n  <author>Per Bothner</author>\n  <author>Kurt Cagle</author>\n  <author>James Linn</author>\n  <author>Vaidyanathan Nagarajan</author>\n  <year>2003</year>\n  <price>49.99</price>\n</book>\n\n<book category=""web"">\n  <title lang=""en"">Learning XML</title>\n  <author>Erik T. Ray</author>\n  <year>2003</year>\n  <price>39.95</price>\n</book>\n\n</bookstore> \n"
      TextAlignment   =   "0"
      TextColor       =   "&h00000000"
      TextFont        =   ""
      TextSize        =   0
      Top             =   28
      Visible         =   True
      Width           =   280.0
   End
   Begin iOSButton Button1
      AccessibilityHint=   ""
      AccessibilityLabel=   ""
      AutoLayout      =   Button1, 1, TextArea1, 1, False, +1.00, 4, 1, 0, , True
      AutoLayout      =   Button1, 7, , 0, False, +1.00, 4, 1, 100, , True
      AutoLayout      =   Button1, 8, , 0, False, +1.00, 4, 1, 30, , True
      AutoLayout      =   Button1, 4, Button3, 3, False, +1.00, 4, 1, -*kStdControlGapV, , True
      Caption         =   "ImportNode"
      Enabled         =   True
      Height          =   30.0
      Left            =   20
      LockedInPosition=   False
      Scope           =   0
      TextColor       =   "&c007AFF00"
      TextFont        =   ""
      TextSize        =   0
      Top             =   404
      Visible         =   True
      Width           =   100.0
   End
   Begin iOSButton Button2
      AccessibilityHint=   ""
      AccessibilityLabel=   ""
      AutoLayout      =   Button2, 2, TextArea1, 2, False, +1.00, 4, 1, 0, , True
      AutoLayout      =   Button2, 7, , 0, False, +1.00, 4, 1, 100, , True
      AutoLayout      =   Button2, 11, Button1, 11, False, +1.00, 4, 1, , , True
      AutoLayout      =   Button2, 8, , 0, False, +1.00, 4, 1, 30, , True
      Caption         =   "Test Parse"
      Enabled         =   True
      Height          =   30.0
      Left            =   200
      LockedInPosition=   False
      Scope           =   0
      TextColor       =   "&c007AFF00"
      TextFont        =   ""
      TextSize        =   0
      Top             =   404
      Visible         =   True
      Width           =   100.0
   End
   Begin iOSButton Button3
      AccessibilityHint=   ""
      AccessibilityLabel=   ""
      AutoLayout      =   Button3, 1, <Parent>, 1, False, +1.00, 4, 1, 20, , True
      AutoLayout      =   Button3, 2, Button2, 2, False, +1.00, 4, 1, 0, , True
      AutoLayout      =   Button3, 8, , 0, False, +1.00, 4, 1, 30, , True
      AutoLayout      =   Button3, 4, BottomLayoutGuide, 4, False, +1.00, 4, 1, -*kStdControlGapV, , True
      Caption         =   "TestGetCustomer"
      Enabled         =   True
      Height          =   30.0
      Left            =   20
      LockedInPosition=   False
      Scope           =   0
      TextColor       =   "&c007AFF00"
      TextFont        =   ""
      TextSize        =   0
      Top             =   442
      Visible         =   True
      Width           =   280.0
   End
End
#tag EndIOSView

#tag WindowCode
	#tag Method, Flags = &h0
		Sub Handle_Error(caller as FSAPI.Request, HTTPStatus as integer, error as text)
		  System.DebugLog( "Error #: " + HTTPStatus.ToText + " " + error )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Handle_ResponseReceived(caller as FSAPI.Request, APIResponse as FSAPI.Response, HTTPStatus as Integer)
		  Dim UBound As Integer = APIResponse.Records.LastRowIndex
		  For i As Integer = 0 To UBound
		    Dim record As FSAPI.Record = APIResponse.Records( i )
		    Dim id() As FSAPI.Field = record.Field( "idCON" )
		    Break
		    //do shit with each record here
		  Next
		  System.DebugLog( Integer( ubound + 1 ).ToText + " records received." )
		End Sub
	#tag EndMethod


	#tag Property, Flags = &h0
		APIRequest As FSAPI.Request
	#tag EndProperty


#tag EndWindowCode

#tag Events Button1
	#tag Event
		Sub Action()
		  Dim doc1 As New FSCustom.XMLDocument( TextArea1.Text )
		  Dim doc2 As New FSCustom.XMLDocument
		  Call doc2.ImportNode( doc1.DocumentElement )
		  Call doc2.ImportNode( doc1.DocumentElement, True )
		  Break
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events Button2
	#tag Event
		Sub Action()
		  Dim node As New XMLDocument( TextArea1.Text )
		  Dim output As Text = node.ToText
		  break
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events Button3
	#tag Event
		Sub Action()
		  APIRequest = New FSAPI.Request( fsapi.Sector.CRM, "GetCustomer" )
		  
		  Call APIRequest.AddParameter( "idContact", "4058108" )
		  
		  AddHandler APIRequest.ResponseReceived, AddressOf Handle_ResponseReceived
		  AddHandler APIRequest.Error, AddressOf handle_Error
		  
		  //Use the key that is appropriate for the app. eventually I will have OAuth set up and each session will have their own key
		  APIRequest.AddAuthToken( "Basic", "fnL26KpvAbAVKLvA67!7*wGiY9ZntvgH79u73a7mJ.CbqZn6B9A6atUtsEyNf*fu" )
		  
		  APIRequest.Send( "https://api.myfrontsight.com" )
		End Sub
	#tag EndEvent
#tag EndEvents
