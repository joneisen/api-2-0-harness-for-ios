#tag Class
Protected Class Response
	#tag Method, Flags = &h0
		Sub Constructor(XMLText as Text)
		  mSource = New XmlDocument( XMLText )
		  
		  resultset = mSource.Xql( "/APIResponse/resultset" ).Item( 0 )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function Records() As FSAPI.Record()
		  Dim output() As FSAPI.Record
		  
		  Dim focusRecord As XmlNode
		  For i As Integer = 0 To resultset.ChildCount - 1
		    focusRecord = resultset.Child( i )
		    Dim record As New FSAPI.Record( focusRecord )
		    output.Append( record )
		  Next
		  
		  Return output
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function Records(index as Integer) As FSAPI.Record
		  If index > ResultSet.ChildCount - 1 Then
		    Return Nil
		  End If
		  Return New FSAPI.Record( ResultSet.Child( index ) )
		End Function
	#tag EndMethod


	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  Return Integer.FromText( resultset.GetAttribute( "fetch-size" ) )
			End Get
		#tag EndGetter
		Count As Integer
	#tag EndComputedProperty

	#tag Property, Flags = &h21
		Private mSource As XmlDocument
	#tag EndProperty

	#tag Property, Flags = &h21
		Private ResultSet As XmlNode
	#tag EndProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  Return Integer.FromText( resultset.GetAttribute( "count" ) )
			End Get
		#tag EndGetter
		ResultSize As Integer
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  Return mSource
			End Get
		#tag EndGetter
		Source As XmlDocument
	#tag EndComputedProperty


	#tag ViewBehavior
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			InitialValue=""
			Type="String"
			EditorType=""
		#tag EndViewProperty
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
			EditorType=""
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			InitialValue=""
			Type="String"
			EditorType=""
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
			EditorType=""
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
			EditorType=""
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=false
			Group="Behavior"
			InitialValue=""
			Type="String"
			EditorType=""
		#tag EndViewProperty
		#tag ViewProperty
			Name="ResultSize"
			Visible=false
			Group="Behavior"
			InitialValue=""
			Type="Integer"
			EditorType=""
		#tag EndViewProperty
		#tag ViewProperty
			Name="Count"
			Visible=false
			Group="Behavior"
			InitialValue=""
			Type="Integer"
			EditorType=""
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
