#tag Class
Protected Class RequestParameter
	#tag Method, Flags = &h0
		Sub AddValue(value as Text, Optional Oper as Text)
		  Dim node As XmlNode = parent.CreateElement( "Value" )
		  node.AppendChild( Parent.CreateTextNode( value ) )
		  If oper <> "" Then
		    node.SetAttribute( "Operator", oper )
		  End If
		  source.AppendChild( node )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Constructor(ParameterName as Text, ParameterValue() as Text, RequestParent as XmlDocument, Optional ParameterOperators() as FSAPI.Operator)
		  If ParameterName = "" Or RequestParent Is Nil Then
		    Raise New InvalidArgumentException
		  End If
		  
		  Parent = RequestParent
		  source = Parent.CreateElement( "Parameter" )
		  Dim name As XmlNode = parent.CreateElement( "Name" )
		  name.AppendChild( Parent.CreateTextNode( ParameterName ) )
		  source.AppendChild( name )
		  If ParameterOperators <> Nil Then
		    If ParameterOperators.LastRowIndex <> ParameterValue.LastRowIndex Then
		      ParameterOperators = Nil
		    End If
		  End If
		  For i As Integer = 0 To ParameterValue.LastRowIndex
		    If ParameterOperators = Nil Then
		      AddValue( ParameterValue( i ) )
		    Else
		      AddValue( ParameterValue( i ), ParameterOperators( i ).ToText )
		    End If
		  Next
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub SetOperator(valueIndex as Integer, oper as FSAPI.Operator)
		  Dim list As XmlNodeList = source.Xql( "/Parameter/Value" )
		  
		  Dim node As XmlNode = list.Item( valueIndex )
		  
		  node.SetAttribute( "Operator", oper.ToText )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function ToXML(parent as XmlDocument) As XmlNode
		  Return parent.ImportNode( source, true )
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function Value() As Text()
		  Dim list As XmlNodeList = source.Xql( "/Parameter/Value" )
		  Dim output() As Text
		  For i As Integer = 0 To list.Length - 1
		    output.append( list.Item( i ).FirstChild.Value )
		  Next
		  Return output
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function Value(index as Integer) As Text
		  Return Value( index )
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Value(index as integer, assigns value as Text)
		  Dim list As XmlNodeList = Source.Xql( "/Parameter/Value" )
		  
		  list.Item( index ).FirstChild.Value = value
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function ValueCount() As Integer
		  Dim list As XmlNodeList = source.Xql( "/Parameter/Value" )
		  Return list.Length
		End Function
	#tag EndMethod


	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  Dim attr As Text = source.GetAttribute( "CaseSensitive" )
			  If attr <> "" Then
			    Return attr.ToBoolean
			  Else
			    Return False
			  End If
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  source.SetAttribute( "CaseSensitive", value.ToText )
			End Set
		#tag EndSetter
		CaseSensitive As Boolean
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  Dim list As XmlNodeList = source.Xql( "/Name" )
			  If list.Length > 0 Then
			    Return list.Item( 0 ).FirstChild.Value
			  End If
			End Get
		#tag EndGetter
		Name As Text
	#tag EndComputedProperty

	#tag Property, Flags = &h21
		Private Parent As XmlDocument
	#tag EndProperty

	#tag Property, Flags = &h21
		Private Source As XmlNode
	#tag EndProperty


	#tag ViewBehavior
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			InitialValue=""
			Type="String"
			EditorType=""
		#tag EndViewProperty
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
			EditorType=""
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			InitialValue=""
			Type="String"
			EditorType=""
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
			EditorType=""
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
			EditorType=""
		#tag EndViewProperty
		#tag ViewProperty
			Name="CaseSensitive"
			Visible=false
			Group="Behavior"
			InitialValue=""
			Type="Boolean"
			EditorType=""
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
