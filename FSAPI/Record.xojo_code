#tag Class
Protected Class Record
	#tag Method, Flags = &h0
		Sub AddField(FieldName as Text, FieldData as Text)
		  Dim newField As New FSAPI.Field
		  newField.FieldName = FieldName
		  newField.Data = FieldData
		  
		  Fields.AddRow( newField )
		  
		  If Not mLockUpdate Then
		    UpdateIndices
		  End If
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Constructor()
		  mKeys = new Xojo.Core.Dictionary
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Constructor(data as XmlNode)
		  Constructor
		  LockUpdate = True
		  Dim focusChild As XmlNode
		  Dim dataNode As XmlNode
		  For i As Integer = 0 To data.ChildCount - 1
		    focusChild = data.Child( i )
		    Dim name As Text = focusChild.GetAttributeNode( "name" ).Value
		    Dim value As Text
		    If focusChild.ChildCount = 1 And focusChild.FirstChild.Name = "data" Then
		      dataNode = focusChild.FirstChild
		      If dataNode.ChildCount = 1 Then
		        value = dataNode.FirstChild.Value
		      End If
		    End If
		    AddField( name, value )
		    dataNode = Nil
		  Next
		  LockUpdate = False
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function Field(name as Text) As FSAPI.Field()
		  Dim output() As FSAPI.Field
		  For i As Integer = 0 To fields.Ubound
		    If fields( i ).FieldName = name Then
		      output.Append( fields( i ) )
		    End If
		  Next
		  
		  return output
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function Keys() As Text()
		  Dim output() As Text
		  For Each entry As Xojo.Core.DictionaryEntry In mKeys
		    output.AddRow( entry.Key )
		  Next
		  output.Sort
		  Return output
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub RemoveField(index as Integer)
		  Dim name As Text = fields( index ).FieldName
		  fields.RemoveRowAt( index )
		  If Not mLockUpdate Then
		    UpdateIndices
		  End If
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub UpdateIndices()
		  mKeys = New Xojo.Core.Dictionary
		  
		  Dim FieldName As Text
		  For i As Integer = 0 To Fields.LastRowIndex
		    FieldName = fields( i ).FieldName
		    If mKeys.HasKey( FieldName ) Then
		      Dim indices() As Integer = mKeys.Value( FieldName )
		      indices.AddRow( i )
		    Else
		      Dim indices() As Integer
		      indices.AddRow( i )
		      mKeys.Value( FieldName ) = indices
		    End If
		  Next
		End Sub
	#tag EndMethod


	#tag Property, Flags = &h0
		Fields() As FSAPI.Field
	#tag EndProperty

	#tag ComputedProperty, Flags = &h21
		#tag Getter
			Get
			  Return mLockUpdate
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  If mLockUpdate And Not value Then
			    UpdateIndices
			  End If
			  mLockUpdate = value
			End Set
		#tag EndSetter
		Private LockUpdate As Boolean
	#tag EndComputedProperty

	#tag Property, Flags = &h21
		Private mKeys As Xojo.Core.Dictionary
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mLockUpdate As Boolean = False
	#tag EndProperty


	#tag ViewBehavior
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			InitialValue=""
			Type="String"
			EditorType=""
		#tag EndViewProperty
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
			EditorType=""
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			InitialValue=""
			Type="String"
			EditorType=""
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
			EditorType=""
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
			EditorType=""
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
