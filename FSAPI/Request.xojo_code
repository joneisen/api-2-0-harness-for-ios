#tag Class
Protected Class Request
Inherits Xojo.Net.HTTPSocket
	#tag Event
		Sub Error(err as RuntimeException)
		  Error( err.ErrorNumber, err.Reason )
		End Sub
	#tag EndEvent

	#tag Event
		Sub PageReceived(URL as Text, HTTPStatus as Integer, Content as xojo.Core.MemoryBlock)
		  #Pragma Unused URL
		  If HTTPStatus >= 200 And HTTPStatus < 300 Then
		    Dim theResponse As New FSAPI.Response( xojo.Core.TextEncoding.UTF8.ConvertDataToText( content ) )
		    ResponseReceived( theResponse, HTTPStatus )
		  Else
		    Error( HTTPStatus, TextEncoding.UTF8.ConvertDataToText( content ) )
		  End If
		End Sub
	#tag EndEvent


	#tag Method, Flags = &h0
		Sub AddAuthToken(AuthType as Text, AuthToken as Text)
		  Self.RequestHeader( "Authorization" ) = AuthType + " " + AuthToken
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function AddParameter(ParameterName as Text, ParameterValue() as Text, optional Operators() as FSAPI.Operator) As FSAPI.RequestParameter
		  Dim newParameter As FSAPI.RequestParameter
		  If Operators = Nil Then
		    newParameter = New FSAPI.RequestParameter( ParameterName, ParameterValue, Source )
		  Else
		    newParameter = New FSAPI.RequestParameter( ParameterName, ParameterValue, Source, Operators )
		  End If
		  Parameters.AddRow( newParameter )
		  Return newParameter
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function AddParameter(ParameterName as Text, ParamArray ParameterValue as Text) As FSAPI.RequestParameter
		  Return AddParameter( ParameterName, ParameterValue )
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Constructor(RequestSector as FSAPI.Sector, RequestName as Text)
		  ////////////////////////////////////////////////////////////////////////////////
		  // By: Jon Eisen ; Front Sight
		  // Created: 2020-05-12
		  // Purpose: Set up the request object
		  // Assumptions: N/A
		  // Modification Explanations:
		  // Script Management Functions [e.g. RemoveHandler]: N/A
		  //
		  ////////////////////////////////////////////////////////////////////////////////
		  msource = new XmlDocument
		  mSector = RequestSector
		  mName = RequestName
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Send(URL as Text)
		  // Calling the overridden superclass method.
		  // Note that this may need modifications if there are multiple  choices.
		  // Possible calls:
		  // Send(method As Text, URL As Text, timeout As Integer = 60) -- From Xojo.Net.HTTPSocket
		  
		  Dim body As Text = Source.ToText
		  Self.SetRequestContent( Xojo.Core.TextEncoding.UTF8.ConvertTextToData( body ), "application/xml" )
		  //Self.RequestHeader( "content-length" ) = body.Bytes.ToText
		  Dim theURL As Text = URL + "/" + mSector.ToText + "/" + mName
		  Super.Send("POST", theURL )
		  
		End Sub
	#tag EndMethod


	#tag Hook, Flags = &h0
		Event Error(HTTPStatus as integer, error as Text)
	#tag EndHook

	#tag Hook, Flags = &h0
		Event ResponseReceived(APIResponse as FSAPI.Response, HTTPStatus as Integer)
	#tag EndHook


	#tag Property, Flags = &h0
		mName As Text
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mSector As Sector
	#tag EndProperty

	#tag Property, Flags = &h0
		mSource As XmlDocument
	#tag EndProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  Return mName
			End Get
		#tag EndGetter
		Name As Text
	#tag EndComputedProperty

	#tag Property, Flags = &h21
		Private Parameters() As FSAPI.RequestParameter
	#tag EndProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  Return mSector
			End Get
		#tag EndGetter
		Sector As Sector
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  mSource = New XMLDocument
			  Dim root As XmlNode = mSource.CreateElement( "APIRequest" )
			  Dim params As XmlNode = mSource.CreateElement( "Parameters" )
			  mSource.AppendChild( root )
			  root.AppendChild( params )
			  For i As Integer = 0 To Parameters.LastRowIndex
			    params.AppendChild( parameters( i ).ToXML( mSource ) )
			  Next
			  
			  Return mSource
			End Get
		#tag EndGetter
		Source As XmlDocument
	#tag EndComputedProperty


	#tag ViewBehavior
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			InitialValue=""
			Type="String"
			EditorType=""
		#tag EndViewProperty
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
			EditorType=""
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			InitialValue=""
			Type="String"
			EditorType=""
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
			EditorType=""
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
			EditorType=""
		#tag EndViewProperty
		#tag ViewProperty
			Name="AllowCertificateValidation"
			Visible=false
			Group="Behavior"
			InitialValue=""
			Type="Boolean"
			EditorType=""
		#tag EndViewProperty
		#tag ViewProperty
			Name="HTTPStatusCode"
			Visible=false
			Group="Behavior"
			InitialValue=""
			Type="Integer"
			EditorType=""
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
