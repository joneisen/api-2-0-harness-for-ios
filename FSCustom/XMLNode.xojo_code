#tag Class
Class XMLNode
	#tag Method, Flags = &h0
		Sub AppendChild(NewChild as FSCustom.XMLNode)
		  mChildren.AddRow( NewChild )
		  NewChild.Index = mChildren.LastRowIndex
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function AppendChild(NewChild as FSCustom.XMLNode) As FSCustom.XMLNode
		  mChildren.AddRow( NewChild )
		  NewChild.Index = mChildren.LastRowIndex
		  Return mChildren( mChildren.LastRowIndex )
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function AttributeNames() As Text()
		  Dim output() As Text
		  For Each entry As Xojo.Core.DictionaryEntry In mAttributes
		    output.AddRow( entry.Key )
		  Next
		  Return output
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function AttributeNames(index as integer) As Text
		  Dim Input() As Text = AttributeNames
		  Return input( index )
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub BuildNode(cleanedValue as Text, ByRef focusDict as Xojo.Core.Dictionary, ByRef depth as integer, ByRef template as Xojo.Core.Dictionary)
		  //Set up a new node
		  Dim nodeParts() As Text = cleanedValue.Split( " " )
		  If focusDict.Value( "NodeName" ) <> Nil Then
		    //We need to go down at least one level
		    If focusDict.Value( "NodeChildren" ) = Nil Then
		      //Check to see if there are children, if not create a new dict
		      focusDict.Value( "NodeChildren" ) = New Xojo.Core.Dictionary
		    End If
		    Dim parent As Xojo.Core.Dictionary = focusDict
		    focusDict = focusDict.Value( "NodeChildren" )
		    focusDict.Value( focusDict.Count ) = template.Clone
		    focusDict = focusDict.Value( focusDict.Count - 1 )
		    focusDict.Value( "Parent" ) = parent
		    depth = depth + 1
		  End If
		  focusDict.Value( "NodeName" ) = nodeParts( 0 )
		  If nodeParts.LastRowIndex > 0 Then
		    //We have attributes
		    If focusDict.Value( "NodeAttributes" ) = Nil Then
		      focusDict.Value( "NodeAttributes" ) = New Xojo.Core.Dictionary
		    End If
		    Dim nodeAttributes As Xojo.Core.Dictionary = focusDict.Value( "NodeAttributes" )
		    For j As Integer = 1 To nodeParts.LastRowIndex
		      Dim microParts() As Text = nodeParts( j ).Split( "=" )
		      If microParts( 1 ).Right( 1 ) <> """" And microParts( 1 ).Right( 1 ) <> "'" Then
		        //We have a malformed attribute
		        Exit For j
		      End If
		      nodeAttributes.Value( microParts( 0 ) ) = microParts( 1 ).ReplaceAll( """", "" ).ReplaceAll( "'", "" )
		    Next
		  End If
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub BuildTextNode(focusLine as text, byref focusDict as Xojo.Core.Dictionary, byref depth as integer, byref template as Xojo.Core.Dictionary)
		  focusLine = focusLine.ReplaceAll( "&#10", Text.FromUnicodeCodepoint( 10 ) )
		  focusLine = focusLine.ReplaceAll( "&#13", Text.FromUnicodeCodepoint( 13 ) )
		  If focusDict.Value( "NodeChildren" ) = Nil Then
		    //Check to see if there are children, if not create a new dict
		    focusDict.Value( "NodeChildren" ) = New Xojo.Core.Dictionary
		  End If
		  Dim parent As Xojo.Core.Dictionary = focusDict
		  focusDict = focusDict.Value( "NodeChildren" )
		  focusDict.Value( focusDict.Count ) = template.Clone
		  focusDict = focusDict.Value( focusDict.Count - 1 )
		  focusDict.Value( "Parent" ) = parent
		  depth = depth + 1
		  focusDict.Value( "NodeValue" ) = focusLine
		  focusDict = focusDict.Value( "Parent" )
		  depth = depth - 1
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function Child(index as integer) As FSCustom.XMLNode
		  Return mChildren( index )
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function Clone(deep as Boolean) As FSCustom.XMLNode
		  Dim newNode As New FSCustom.XMLNode
		  If deep Then
		    newNode.Parse( Me.ToText )
		  Else
		    newNode.Name = Name
		    Dim focusAttribute As FSCustom.XMLAttribute
		    For i As Integer = 0 To newnode.AttributeCount - 1
		      focusAttribute = GetAttributeNode( i )
		      newNode.SetAttribute( focusAttribute.Name, focusAttribute.Value )
		    Next
		  End If
		  Return newNode
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function Compare(NodeToCompare as FSCustom.XMLNode) As Integer
		  Return self.ToText.Compare( NodeToCompare.ToText )
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub Construct(rawData as Xojo.Core.Dictionary, parent as FSCustom.XMLNode)
		  //set parent and owner
		  Self.mParent = parent
		  If parent <> Nil Then
		    mOwnerDocument = parent.OwnerDocument
		  Else
		    If Self IsA FSCustom.XMLDocument Then
		      mOwnerDocument = FSCustom.XMLDocument( Self )
		      Dim intermediate As New FSCustom.XMLNode( rawData, Self )
		      Dim rootNode As FSCustom.XMLElement
		      rootNode = FSCustom.XMLDocument( Self ).CreateElement( intermediate.Name )
		      FSCustom.XMLNode( rootNode ).Construct( rawData, Self )
		      Self.AppendChild( rootNode )
		      Return
		    Else
		      //fuck
		      Break
		    End If
		  End If
		  
		  Dim children As Xojo.Core.Dictionary
		  
		  For Each entry As Xojo.Core.DictionaryEntry In rawData
		    If entry.Key = "NodeName" Then
		      If entry.Value <> Nil Then
		        Me.Name = entry.Value
		      End If
		    Elseif entry.Key = "NodeAttributes" Then
		      If entry.Value <> Nil Then
		        Dim attrs As Xojo.Core.Dictionary = entry.Value
		        For Each attr As Xojo.Core.DictionaryEntry In attrs
		          Self.SetAttribute( attr.Key, attr.Value )
		        Next
		      End If
		    Elseif entry.Key = "NodeChildren" Then
		      If entry.Value <> Nil Then
		        children = entry.Value
		        If children <> Nil Then
		          For Each child As Xojo.Core.DictionaryEntry In children
		            Dim rawChild As Xojo.Core.Dictionary = child.Value
		            Dim intermediate As New FSCustom.XMLNode( rawChild, Self )
		            Dim xChild As XmlNode
		            If intermediate.Name <> "" Then
		              xChild = Self.OwnerDocument.CreateElement( intermediate.Name )
		            Else
		              //Check to see if this is straight text or CDATA
		              If intermediate.Value.Length >= 9 And intermediate.Value.Left( 9 ) = kOpenCarat + kExclamationMark + kOpenBracket + "CDATA" + kOpenBracket Then
		                xChild = Self.OwnerDocument.CreateCDATASection( intermediate.Value )
		              Elseif intermediate.Value.Length >= 4 And intermediate.Value.Left( 4 ) = kOpenCarat + kExclamationMark + kHyphen + kHyphen Then
		                xChild = self.OwnerDocument.CreateComment( intermediate.Value )
		              Else
		                xChild = Self.OwnerDocument.CreateTextNode( intermediate.Value )
		              End If
		            End If
		            
		            FSCustom.XMLNode( xChild ).Construct( rawChild, Self )
		            Self.AppendChild( xChild )
		          Next
		        End If
		      End If
		    Elseif entry.Key = "NodeValue" Then
		      If entry.Value <> Nil Then
		        Self.Value = entry.Value
		      End If
		    Elseif entry.Key = "Parent" Then
		      //ignore
		    Elseif entry.Key = "DocumentProlog" Then
		      //ignore for now
		    Else
		      Break
		      //fuck
		      
		    End If
		  Next
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Constructor()
		  mAttributes = New Xojo.Core.Dictionary
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub Constructor(Name as Text)
		  Constructor()
		  Self.Name = Name
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub Constructor(name as text, value as Text)
		  Constructor( name )
		  Self.Value = value
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub Constructor(URI as Text, Name as Text, value as Text)
		  Constructor( name, value )
		  mNamespaceURI = URI
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Sub Constructor(rawData as Xojo.Core.Dictionary, parent as FSCustom.XMLNode)
		  Constructor
		  //To be used by parser
		  Construct( rawData, parent )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Function CreateAttribute(name as text, value as Text) As FSCustom.XMLAttribute
		  Dim newAttribute As New XMLAttribute
		  newAttribute.Name = name
		  newAttribute.Value = Value
		  Return newAttribute
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function GetAttribute(Name as Text) As Text
		  Dim attr As FSCustom.XMLAttribute = GetAttributeNode( name )
		  If attr Is Nil Then
		    Return ""
		  Else
		    Return attr.ToText
		  End If
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function GetAttributeNode(index as integer) As FSCustom.XMLAttribute
		  Dim attr As FSCustom.XMLAttribute = GetAttributeNode( AttributeNames( index ) )
		  If attr IsA FSCustom.XMLAttribute Then
		    Return attr
		  Else
		    Return Nil
		  End If
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function GetAttributeNode(Name as Text) As FSCustom.XMLAttribute
		  Dim attr As Auto = mAttributes.Lookup( Name, "" )
		  If attr IsA FSCustom.XMLAttribute Then
		    Return attr
		  Else
		    Return Nil
		  End If
		End Function
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Function ImportNode(foreignNode as FSCustom.XMLNode, newParent as FSCustom.XMLDocument, Optional deep as Boolean) As FSCustom.XMLNode
		  Dim newNode As FSCustom.XMLNode
		  If foreignNode IsA FSCustom.XMLTextNode Then
		    newNode = newParent.CreateTextNode( foreignNode.Value )
		  Else
		    newNode = newParent.CreateElement( foreignNode.Name )
		    Dim focusAttr As FSCustom.XMLAttribute
		    For Each attr As Xojo.Core.DictionaryEntry In foreignNode.mAttributes
		      focusAttr = attr.Value
		      newNode.SetAttributeNode( focusAttr )
		    Next
		    If deep Then
		      Dim newChild As FSCustom.XMLNode
		      For i As Integer = 0 To foreignNode.ChildCount - 1
		        newChild = newNode.ImportNode( foreignNode.Child( i ), newParent, deep )
		      Next
		    End If
		  End If
		  If Not Self IsA FSCustom.XMLDocument Then
		    Self.AppendChild( newNode )
		  End If
		  Return newNode
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function Insert(NewChild as FSCustom.XMLNode, RefChild as FSCustom.XMLNode) As FSCustom.XMLNode
		  mChildren.AddRowAt( RefChild.Index, NewChild )
		  NewChild.Index = RefChild.Index
		  For i As Integer = RefChild.Index + 1 To mChildren.LastRowIndex
		    mChildren( i ).Index = i
		  Next
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function Liner(XMLString as Text) As Text()
		  Dim lastChar As Integer
		  Dim inner(), output() As Text
		  Dim Write, inNode, inQuote, isCDATA As Boolean
		  
		  //Clean Smart Quotes
		  XMLString.CleanSmartQuotes
		  
		  For Each char As Integer In XMLString.Codepoints
		    //0 to 31 and 127 to 159 are control characters, ignore all except returns
		    If char <> 10 And char <> 13 And ( char < 32 Or ( char > 126 And char < 160 ) ) Then
		      Continue
		    End If
		    
		    If inNode And Not inQuote Then
		      //ignore spaces between = and " in nodes while not in quotes, also only one space between attributes
		      If char.isUnicodeSpace Then
		        If lastChar = kEqualSignPoint Or lastChar.isUnicodeSpace Then
		          Continue For char
		        End If
		        If lastChar = kOpenCaratPoint Or lastChar = kExclamationMarkPoint Then
		          Continue For char
		        End If
		      End If
		      //If char = 10 Or char = 13 Then
		      //
		      //End If
		    End If
		    If inNode And Not inQuote And char = kEqualSignPoint Then
		      For i As Integer = inner.LastRowIndex To 0
		        For Each point As Integer In inner( i ).Codepoints
		          If point.isUnicodeSpace Then
		            Call inner.pop
		          Else
		            Exit For i
		          End If
		        Next
		      Next
		    End If
		    
		    
		    //60 = "<" and 62 = ">"
		    If lastChar = kCloseCaratPoint Then
		      If isCDATA Then
		        If inner( inner.LastRowIndex - 1 ) = kCloseBracket Then
		          Write = True
		        End If
		      Else
		        write = True
		      End If
		    End If
		    //tell the liner to treat CDATA as text
		    //91 = "[" and 33 = "!"
		    //if there are only two existing chars in inner() < and !
		    If char = kOpenBracketPoint And lastchar = kExclamationMarkPoint And inNode And inner.LastRowIndex = 1 Then
		      isCDATA = True
		      inQuote = True
		    End If
		    Dim pooped As Text
		    If lastChar = kOpenCaratPoint And char = kForwardSlashPoint And inner.LastRowIndex > 0 And Not isCDATA Then
		      pooped = inner.Pop
		      write = True
		    End If
		    If write Then
		      //Test to see if this is a node or not
		      Dim newLine As Text
		      If inner.IndexOf( "<" ) > -1 Then
		        //we are in an node
		        newLine = Text.Join( inner, "" ).Trim
		      Else
		        //look back to make sure we are in a valid place for a text node
		        Dim lastline As Text = output( output.LastRowIndex )
		        If lastline.Left( 2 ) = kOpenCarat + kForwardSlash Or lastline.Right( 2 ) = kForwardSlash + kCloseCarat Then
		          write = False
		        End If
		        newLine = Text.Join( inner, "" )
		      End If 
		      
		      If write Then
		        output.AddRow( newLine )
		      End If
		      write = False
		      inNode = False
		      inQuote = False
		      isCDATA = False
		      inner.ResizeTo( -1 )
		    End If
		    If pooped <> "" Then
		      inner.AddRow( pooped )
		      pooped = ""
		    End If
		    If ( char = 10 Or char = 13 ) Then
		      If inner.LastRowIndex > 0 Then
		        If inNode Then
		          inner.AddRow( Text.FromUnicodeCodepoint( 32 ) )
		        Else
		          inner.AddRow( "&#" + char.ToText )
		        End If
		      End If
		    Else
		      inner.AddRow( Text.FromUnicodeCodepoint( char ) )
		      
		    End If
		    //Check if we're in a node
		    If inner.LastRowIndex = 0 Or inner.LastRowIndex = 1 Then//in case we popped and wrote two characters this run
		      If inner( 0 ) = kOpenCarat Then
		        inNode = True
		      End If
		    Else
		      If inNode And char = kCloseCaratPoint Then
		        inNode = False
		      End If
		    End If
		    If inNode Then
		      If char = kDoubleQuotePoint Or char = kSingleQuotePoint Then
		        //switch inQuote boolean
		        inQuote = Not inQuote
		      End If
		    End If
		    lastChar = char
		  Next
		  //Last node
		  Dim writeLast As Boolean = False
		  //check to make sure there is something more than just spaces
		  For i As Integer = 0 To inner.LastRowIndex
		    If inner( i ) <> Text.FromUnicodeCodepoint( 32 ) Then
		      writeLast = True
		      Exit For i
		    End If
		  Next
		  
		  If writeLast Then
		    //Test to see if this is a node or not
		    Dim start, stop As Integer
		    Dim newLine As Text
		    start = inner.IndexOf( "<" )
		    If start > -1 Then
		      stop = inner.IndexOf( ">" )
		      If start < stop Then
		        //we are in an node
		        newLine = Text.Join( inner, "" ).Trim
		      Else
		        newLine = Text.Join( inner, "" )
		      End If
		    Else
		      newLine = Text.Join( inner, "" )
		    End If 
		    
		    output.AddRow( newLine )
		  End If
		  Return output
		End Function
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Sub Parse(XMLString as Text)
		  Dim rawData As Xojo.Core.Dictionary = Tokenize( XMLString )
		  
		  Constructor( rawData, nil )
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function RemoveAttributeNode(attributeNode as FSCustom.XMLAttribute) As FSCustom.XMLAttribute
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub RemoveChild(OldChild as FSCustom.XMLNode)
		  mChildren.RemoveRowAt( OldChild.Index )
		  For i As Integer = OldChild.Index To mChildren.LastRowIndex
		    mChildren( i ).Index = i
		  Next
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub ReplaceChild(NewChild as FSCustom.XMLNode, OldChild as FSCustom.XMLNode)
		  mChildren( OldChild.Index ) = NewChild
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function ReplaceChild(NewChild as FSCustom.XMLNode, OldChild as FSCustom.XMLNode) As FSCustom.XMLNode
		  mChildren( OldChild.Index ) = NewChild
		  Return NewChild
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub SetAttribute(Name as Text, Value as Text)
		  mAttributes.Value( name ) = CreateAttribute( name, value )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub SetAttributeNode(AttributeNode as FSCustom.XMLAttribute)
		  mAttributes.Value( AttributeNode.Name ) = CreateAttribute( AttributeNode.Name, AttributeNode.Value )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function Tokenize(XMLString as Text) As Xojo.Core.Dictionary
		  ////////////////////////////////////////////////////////////////////////////////
		  // By: Jon Eisen ; Front Sight
		  // Created: 2020-05-14
		  // Purpose: Turn XML String into a Dictionary of dictionaries
		  // Assumptions: N/A
		  // Modification Explanations: Please don't
		  // Script Management Functions [e.g. RemoveHandler]: N/A
		  //
		  ////////////////////////////////////////////////////////////////////////////////
		  
		  //Set up the default dict
		  Dim template As New Xojo.Core.Dictionary
		  template.Value( "NodeName" ) = Nil
		  template.Value( "NodeAttributes" ) = Nil
		  template.Value( "NodeChildren" ) = Nil
		  template.Value( "NodeValue" ) = Nil
		  template.Value( "Parent" ) = Nil
		  Dim output As Xojo.Core.Dictionary = template.Clone
		  Dim focusDict As Xojo.Core.Dictionary = output
		  
		  Dim lines() As Text = liner( XMLString )
		  
		  Dim focusLine, first2, last2, cleanedValue, nodeParts() As Text
		  
		  Dim depth As Integer = 0
		  
		  For i As Integer = 0 To lines.LastRowIndex
		    focusLine = lines( i )
		    //handle short xmltextnodes
		    If focusLine.Trim.Length = 0 Then
		      Continue For i
		    End If
		    If focusLine.Length < 2 Then
		      first2 = focusLine.Left( focusLine.Length )
		      last2 = focusLine.Right( focusLine.Length )
		    Else
		      first2 = focusLine.Left( 2 )
		      last2 = focusLine.Right( 2 )
		    End If
		    If first2.Length >= 2 And first2.Left( 1 ) = kOpenCarat Then
		      //This is the opening of a node tag
		      focusLine = focusLine.ReplaceAll( "&#10", "" )
		      focusLine = focusLine.ReplaceAll( "&#13", "" )
		      If first2.Right( 1 ) = kForwardSlash Then //this should be an end node
		        If last2.Right( 1 ) = kCloseCarat Then //this should be a valid end node
		          If last2.Left( 1 ) <> kForwardSlash Then
		            //it is
		            //Do shit here to end a node
		            //Step up one level
		            Dim parent As Xojo.Core.Dictionary = focusDict.Value( "Parent" )
		            If parent <> Nil Then
		              cleanedValue = focusLine.Mid( 2, focusLine.Length - 3 )
		              nodeParts = cleanedValue.Split( " " )
		              If nodeParts( 0 ).Compare( parent.Value( "NodeName" ), Text.CompareCaseSensitive ) <> 0 Then
		                //we have a malformed node
		              End If
		              focusDict = parent
		              depth = depth - 1
		            Else
		              //this is the end of the document
		              Exit For i
		            End If
		          Else
		            //we have a malformed node
		          End If
		        Else
		          //we have a malformed node
		        End If
		      Elseif first2.Right( 1 ) = kQuestionMark Then //this should be the doc prolog
		        If last2.Right( 1 ) = kCloseCarat And focusLine.Left( 5 ) = "<?xml" Then
		          //This is a valid doc prolog
		          //short doc prologs do not have a question mark preceding the closing carat
		          //do shit here to load the doc prolog
		          //xml standard requires doc prolog must be the first line
		          If depth > 0 Then //if deeper than root, ignore
		            Continue For i
		          End If
		          focusDict.Value( "DocumentProlog" ) = template.Clone
		          Dim parent As Xojo.Core.Dictionary = focusDict
		          focusDict = focusDict.Value( "DocumentProlog" )
		          focusDict.Value( "Parent" ) = parent
		          depth = depth + 1
		          //focusDict.Value( "NodeName" ) = "DocumentProlog"
		          If last2.Left( 1 ) = kQuestionMark Then //This is a longer doc prolog
		            cleanedValue = focusLine.Mid( 2, focusLine.Length - 4 )
		          Else
		            cleanedValue = focusLine.Mid( 2, focusLine.Length - 3 )
		          End If
		          BuildNode( cleanedValue, focusDict, depth, template )
		          focusDict = focusDict.Value( "Parent" )
		          depth = depth - 1
		        Else
		          //we have a malformed node
		        End If
		      Elseif first2.Right( 1 ) = kExclamationMark Then
		        //we have DTD, CDATA or a comment
		        #Pragma Warning "TODO: implement comments, CDATA and DTD parsing"
		        If focusLine.Left( 4 ) = kOpenCarat + kExclamationMark + kHyphen + kHyphen Then
		          //We have a comment
		          BuildTextNode( focusLine, focusDict, depth, template )
		        Elseif focusLine.Left( 9 ) = kOpenCarat + kExclamationMark + "DOCTYPE" Then
		          //We have a DTD
		        Elseif focusLine.Left( 9 ) = kOpenCarat + kExclamationMark + kOpenBracket + "CDATA" + kOpenBracket Then
		          //we have cdata
		          BuildTextNode( focusLine, focusDict, depth, template )
		        Else
		          //fuck
		          Break
		        End If
		      Else // not a closing node tag or a doc prolog tag
		        //Test the end
		        If last2.Right( 1 ) = kCloseCarat Then
		          Dim deadEnd As Boolean = False
		          If last2.Left( 1 ) = kForwardSlash Then
		            //This is a dead end node
		            cleanedValue = focusLine.Mid( 1, focusLine.Length - 3 )
		            deadEnd = True
		          Else
		            //End of the opening node
		            cleanedValue = focusLine.Mid( 1, focusLine.Length - 2 )
		          End If
		          //Set up a new node
		          BuildNode( cleanedValue, focusDict, depth, template )
		          //End If
		          If deadEnd Then
		            focusDict = focusDict.Value( "Parent" )
		            depth = depth - 1
		          End If
		        Else
		          //we have a malformed node
		        End If
		      End If
		    Else
		      //This is a text node...probably
		      BuildTextNode( focusLine, focusDict, depth, template )
		    End If
		    
		  Next
		  
		  Return output
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function XPath(query as text, refPoint as FSCustom.XMLNode = Nil, optional map() as Text) As FSCustom.XMLNode()
		  ////////////////////////////////////////////////////////////////////////////////
		  // By: Jon Eisen ; Front Sight
		  // Created: 2020-05-19
		  // Purpose: Perform XQL (XPath) queries on Self
		  // Assumptions: N/A
		  // Modification Explanations:
		  // Script Management Functions [e.g. RemoveHandler]: N/A
		  //
		  ////////////////////////////////////////////////////////////////////////////////
		  
		  Dim output() As FSCustom.XMLNode
		  Dim isAbsolute, anywhere As Boolean
		  Dim queries(), queryParts() As Text
		  
		  queries = query.Split( "|" )
		  For i As Integer = 0 To queries.LastRowIndex
		    //reset variables
		    isAbsolute = False
		    anywhere = False
		    
		    //split it on forward slash
		    // for every line of text in this array there was a slash in the query
		    // so /bookstore would show up as a single row, but //bookstore would have one blank row at 0 and one row "bookstore" at 1
		    // if a line in this array is blank that means it was the first of a double slash
		    queryParts = queries( i ).Split( "/" )
		    
		    //if a query begins with a forward slash it is absolute not relative
		    If queryParts( 0 ).Empty Then
		      isAbsolute = True
		      If queryParts( 1 ).Empty Then
		        //this started with //
		        anywhere = True
		        queryParts.RemoveRowAt( 1 )
		      End If
		      queryParts.RemoveRowAt( 0 )
		    End If
		    If Self IsA FSCustom.XMLDocument Or isAbsolute Then
		      If refPoint = Nil Then
		        refPoint = Self.OwnerDocument.DocumentElement
		      End If
		    End If
		    If refPoint.Name.Compare( queryParts( 0 ), Text.CompareCaseSensitive ) <> 0 Then
		      If Not anywhere Then
		        Continue For i
		      End If
		    Elseif queryParts.LastRowIndex = 0 Then
		      output.AddRow( Self )
		      If Not anywhere Then
		        Continue For i
		      End If
		    End If
		    
		    Dim childQuery() As Text
		    For k As Integer = 0 To queryParts.LastRowIndex
		      childQuery.AddRow( queryParts( k ) )
		    Next
		    Dim terminalQuery, anyChild As Boolean = False
		    If childQuery.LastRowIndex > 0 Then
		      childQuery.RemoveRowAt( 0 )
		    Else
		      terminalQuery = True
		    End If
		    
		    //check to see if this is an anychild query
		    If childQuery( 0 ).Empty Then
		      anyChild = True
		      childQuery.RemoveRowAt( 0 )
		    End If
		    
		    For j As Integer = 0 To refPoint.ChildCount - 1
		      //check to see if any children's names match the querypart we're looking at
		      Dim match As Integer = refPoint.Child( j ).Name.Compare( childQuery( 0 ), Text.CompareCaseSensitive )
		      If match = 0 Or anywhere Or anyChild Then
		        If anywhere Or anyChild Then
		          childQuery.AddRowAt( 0, "" )
		          childQuery.AddRowAt( 0, "" )
		        End If
		        Dim test() As XmlNode = refPoint.Child( j ).XPath( Text.Join( childQuery, "/" ), refPoint.Child( j ) )
		        If test.LastRowIndex > -1 Then
		          For k As Integer = 0 To test.LastRowIndex
		            output.AddRow( test( k ) )
		          Next
		        End If
		        //End If//
		      End If
		      
		      
		    Next
		  Next
		  
		  Return output
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function XQL(query as Text, optional Map() as Text) As FSCustom.XMLNodeList
		  Dim output As New FSCustom.PrivateXMLNodeList
		  If map = Nil Then
		    Return output.Build( Self.XPath( query, Nil ) )
		  Else
		    Return output.Build( Self.XPath( query, Nil, Map ) )
		  End If
		End Function
	#tag EndMethod


	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  Return mAttributes.Count
			End Get
		#tag EndGetter
		AttributeCount As Integer
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  Return mChildren.LastRowIndex + 1
			End Get
		#tag EndGetter
		ChildCount As Integer
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  If mChildren.LastRowIndex > -1 Then
			    Return mChildren( 0 )
			  Else
			    Return Nil
			  End If
			End Get
		#tag EndGetter
		FirstChild As FSCustom.XMLNode
	#tag EndComputedProperty

	#tag Property, Flags = &h1
		Attributes( hidden ) Protected Index As Integer
	#tag EndProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  If mChildren.LastRowIndex > -1 Then
			    Return mChildren( mChildren.LastRowIndex )
			  Else
			    Return Nil
			  End If
			End Get
		#tag EndGetter
		LastChild As FSCustom.XMLNode
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  Return Integer( mLastError )
			End Get
		#tag EndGetter
		LastError As Integer
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  Return mLocalName
			End Get
		#tag EndGetter
		LocalName As Text
	#tag EndComputedProperty

	#tag Property, Flags = &h1
		Protected mAttributes As Xojo.Core.Dictionary
	#tag EndProperty

	#tag Property, Flags = &h1
		Protected mChildren() As FSCustom.XMLNode
	#tag EndProperty

	#tag Property, Flags = &h1
		Protected mLastError As ErrorCode
	#tag EndProperty

	#tag Property, Flags = &h1
		Protected mLocalName As Text
	#tag EndProperty

	#tag Property, Flags = &h1
		Protected mNamespaceURI As Text
	#tag EndProperty

	#tag Property, Flags = &h1
		Protected mOwnerDocument As FSCustom.XMLDocument
	#tag EndProperty

	#tag Property, Flags = &h1
		Protected mParent As FSCustom.XMLNode
	#tag EndProperty

	#tag Property, Flags = &h1
		Protected mPrefix As Text
	#tag EndProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  If mPrefix <> "" Then
			    Return mPrefix + ":" + mLocalName
			  Else
			    Return LocalName
			  End If
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  Dim part() As Text = value.Split( ":" )
			  If part.LastRowIndex = 0 Then
			    mLocalName = value
			  Elseif part.LastRowIndex = 1 Then //we have a prefix
			    mPrefix = part( 0 )
			    mLocalName = part( 1 )
			  Else
			    mLastError = ErrorCode.NotOK
			  End If
			End Set
		#tag EndSetter
		Name As Text
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  Return mNamespaceURI
			End Get
		#tag EndGetter
		NamespaceURI As Text
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		NextSibling As FSCustom.XMLNode
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  Return mOwnerDocument
			End Get
		#tag EndGetter
		OwnerDocument As FSCustom.XMLDocument
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  Return mParent
			End Get
		#tag EndGetter
		Parent As FSCustom.XMLNode
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  Return mPrefix
			End Get
		#tag EndGetter
		Prefix As Text
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  Dim output() As Text
			  If Self IsA FSCustom.XMLComment Then
			    output.AddRow( "<!" )
			    output.AddRow( "--" )
			    output.AddRow( Me.Value )
			    output.AddRow( "--" )
			    output.AddRow( ">" )
			  Elseif Not Self IsA FSCustom.XMLDocument Then
			    //opening carat
			    output.AddRow( "<" )
			    //node name
			    output.AddRow( Name )
			    
			    //check for xmlns
			    If mNamespaceURI <> "" Then
			      output.AddRow( " " )
			      output.AddRow( "xmlns=""" )
			      output.AddRow( mNamespaceURI )
			      output.AddRow( """" )
			    End If
			    
			    //loop through attributes
			    If mAttributes <> Nil And mAttributes.Count > 0 Then
			      For Each entry As Xojo.Core.DictionaryEntry In mAttributes
			        If entry.Value IsA FSCustom.XMLAttribute Then
			          Dim focusAttribute As FSCustom.XMLAttribute = entry.Value
			          //space
			          output.AddRow( " " )
			          output.AddRow( focusAttribute.ToText )
			        End If
			      Next
			    End If
			    
			    //Check to see if there are children, if not put a slash here to indicate the end of the node
			    If mChildren.LastRowIndex = -1 Then
			      output.AddRow( "/" )
			    End If
			    
			    //closing carat
			    output.AddRow( ">" )
			  End If
			  
			  //Check for children
			  For i As Integer = 0 To mChildren.LastRowIndex
			    output.AddRow( mChildren( i ).ToText )
			  Next
			  
			  If mChildren.LastRowIndex > -1 and not self IsA FSCustom.XMLDocument Then
			    output.AddRow( "</" )
			    output.AddRow( Name )
			    output.AddRow( ">" )
			  End If
			  
			  Dim constructed As Text = Text.Join( output, "" )
			  If constructed = "</>" And Me.Value <> "" Then
			    constructed = Me.Value
			  End If
			  Return constructed
			End Get
		#tag EndGetter
		ToText As Text
	#tag EndComputedProperty

	#tag Property, Flags = &h0
		Value As Text
	#tag EndProperty


	#tag Constant, Name = kCloseBracket, Type = Text, Dynamic = False, Default = \"]", Scope = Private
	#tag EndConstant

	#tag Constant, Name = kCloseBracketPoint, Type = Double, Dynamic = False, Default = \"93", Scope = Private
	#tag EndConstant

	#tag Constant, Name = kCloseCarat, Type = Text, Dynamic = False, Default = \">", Scope = Private
	#tag EndConstant

	#tag Constant, Name = kCloseCaratPoint, Type = Double, Dynamic = False, Default = \"62", Scope = Private
	#tag EndConstant

	#tag Constant, Name = kDoubleQuote, Type = Text, Dynamic = False, Default = \"\"", Scope = Private
	#tag EndConstant

	#tag Constant, Name = kDoubleQuotePoint, Type = Double, Dynamic = False, Default = \"34", Scope = Private
	#tag EndConstant

	#tag Constant, Name = kEmDash, Type = Text, Dynamic = False, Default = \"\xE2\x80\x94", Scope = Private
	#tag EndConstant

	#tag Constant, Name = kEmDashPoint, Type = Double, Dynamic = False, Default = \"8212", Scope = Private
	#tag EndConstant

	#tag Constant, Name = kEqualSign, Type = Text, Dynamic = False, Default = \"\x3D", Scope = Private
	#tag EndConstant

	#tag Constant, Name = kEqualSignPoint, Type = Double, Dynamic = False, Default = \"61", Scope = Private
	#tag EndConstant

	#tag Constant, Name = kExclamationMark, Type = Text, Dynamic = False, Default = \"!", Scope = Private
	#tag EndConstant

	#tag Constant, Name = kExclamationMarkPoint, Type = Double, Dynamic = False, Default = \"33", Scope = Private
	#tag EndConstant

	#tag Constant, Name = kForwardSlash, Type = Text, Dynamic = False, Default = \"/", Scope = Private
	#tag EndConstant

	#tag Constant, Name = kForwardSlashPoint, Type = Double, Dynamic = False, Default = \"47", Scope = Private
	#tag EndConstant

	#tag Constant, Name = kHyphen, Type = Text, Dynamic = False, Default = \"-", Scope = Private
	#tag EndConstant

	#tag Constant, Name = kHyphenPoint, Type = Double, Dynamic = False, Default = \"45", Scope = Private
	#tag EndConstant

	#tag Constant, Name = kOpenBracket, Type = Text, Dynamic = False, Default = \"[", Scope = Private
	#tag EndConstant

	#tag Constant, Name = kOpenBracketPoint, Type = Double, Dynamic = False, Default = \"91", Scope = Private
	#tag EndConstant

	#tag Constant, Name = kOpenCarat, Type = Text, Dynamic = False, Default = \"<", Scope = Private
	#tag EndConstant

	#tag Constant, Name = kOpenCaratPoint, Type = Double, Dynamic = False, Default = \"60", Scope = Private
	#tag EndConstant

	#tag Constant, Name = kQuestionMark, Type = Text, Dynamic = False, Default = \"\?", Scope = Private
	#tag EndConstant

	#tag Constant, Name = kQuestionMarkPoint, Type = Double, Dynamic = False, Default = \"63", Scope = Private
	#tag EndConstant

	#tag Constant, Name = kSingleQuote, Type = Text, Dynamic = False, Default = \"\'", Scope = Private
	#tag EndConstant

	#tag Constant, Name = kSingleQuotePoint, Type = Double, Dynamic = False, Default = \"39", Scope = Private
	#tag EndConstant


	#tag Enum, Name = ErrorCode, Type = Integer, Flags = &h21
		NoError
		  IndexSizeError
		  DOMStringSizeError
		  HierarchyRequestError
		  WrongDocument
		  InvalidCharacter
		  NoDataAllowed
		  NoModificationAllowed
		  NotFound
		  NotSupported
		  InUseAttribute
		  InvalidState
		  SyntaxError
		  InvalidModification
		  NamespaceError
		  InvalidAccess
		  InvalidNodeType
		  QueryParseError
		  QueryExecutionError
		NotOK
	#tag EndEnum


	#tag ViewBehavior
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			InitialValue=""
			Type="String"
			EditorType=""
		#tag EndViewProperty
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue=""
			Type="Integer"
			EditorType=""
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			InitialValue=""
			Type="String"
			EditorType=""
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
			EditorType=""
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
			EditorType=""
		#tag EndViewProperty
		#tag ViewProperty
			Name="Value"
			Visible=false
			Group="Behavior"
			InitialValue=""
			Type="Text"
			EditorType=""
		#tag EndViewProperty
		#tag ViewProperty
			Name="ChildCount"
			Visible=false
			Group="Behavior"
			InitialValue=""
			Type="Integer"
			EditorType=""
		#tag EndViewProperty
		#tag ViewProperty
			Name="LastError"
			Visible=false
			Group="Behavior"
			InitialValue=""
			Type="Integer"
			EditorType=""
		#tag EndViewProperty
		#tag ViewProperty
			Name="LocalName"
			Visible=false
			Group="Behavior"
			InitialValue=""
			Type="Text"
			EditorType=""
		#tag EndViewProperty
		#tag ViewProperty
			Name="Prefix"
			Visible=false
			Group="Behavior"
			InitialValue=""
			Type="Text"
			EditorType=""
		#tag EndViewProperty
		#tag ViewProperty
			Name="NamespaceURI"
			Visible=false
			Group="Behavior"
			InitialValue=""
			Type="Text"
			EditorType=""
		#tag EndViewProperty
		#tag ViewProperty
			Name="ToText"
			Visible=false
			Group="Behavior"
			InitialValue=""
			Type="Text"
			EditorType=""
		#tag EndViewProperty
		#tag ViewProperty
			Name="AttributeCount"
			Visible=false
			Group="Behavior"
			InitialValue=""
			Type="Integer"
			EditorType=""
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
