#tag Class
Class XMLElement
Inherits FSCustom.XMLNode
	#tag Method, Flags = &h1
		Protected Sub Constructor(rawData as Xojo.Core.Dictionary, parent as FSCustom.XMLNode)
		  // Calling the overridden superclass constructor.
		  // Note that this may need modifications if there are multiple constructor choices.
		  // Possible constructor calls:
		  // Constructor() -- From XMLNode
		  // Constructor(Name as Text) -- From XMLNode
		  // Constructor(URI as Text, Name as Text, value as Text) -- From XMLNode
		  // Constructor(rawData as Xojo.Core.Dictionary, parent as FSCustom.XMLNode) -- From XMLNode
		  // Constructor(name as text, value as Text) -- From XMLNode
		  Super.Constructor( rawData, parent )
		  
		End Sub
	#tag EndMethod


	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  Return mAttributes.Count
			End Get
		#tag EndGetter
		AttributeCount As Integer
	#tag EndComputedProperty


	#tag ViewBehavior
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			InitialValue=""
			Type="String"
			EditorType=""
		#tag EndViewProperty
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
			EditorType=""
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			InitialValue=""
			Type="String"
			EditorType=""
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
			EditorType=""
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
			EditorType=""
		#tag EndViewProperty
		#tag ViewProperty
			Name="AttributeCount"
			Visible=false
			Group="Behavior"
			InitialValue=""
			Type="Integer"
			EditorType=""
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
