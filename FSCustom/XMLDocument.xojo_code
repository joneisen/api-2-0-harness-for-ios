#tag Class
Class XMLDocument
Inherits FSCustom.XMLNode
	#tag Method, Flags = &h0
		Sub Constructor(XMLString as Text)
		  // Calling the overridden superclass constructor.
		  // Note that this may need modifications if there are multiple constructor choices.
		  // Possible constructor calls:
		  // Constructor() -- From XMLNode
		  // Constructor(Name as Text) -- From XMLNode
		  // Constructor(URI as Text, Name as Text) -- From XMLNode
		  // Constructor(rawData as Xojo.Core.Dictionary, parent as FSCustom.XMLNode) -- From XMLNode
		  Super.Constructor()
		  
		  Parse( XMLString )
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function CreateAttribute(name as text, value as Text) As FSCustom.XMLAttribute
		  // Calling the overridden superclass method.
		  Return Super.CreateAttribute(name, value)
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function CreateCDATASection(Data as Text) As FSCustom.XMLCDATASection
		  Dim output As New FSCustom.XMLCDATASection
		  
		  output.Value = Data
		  
		  Return output
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function CreateComment(Data as Text) As FSCustom.XMLComment
		  Dim output As New FSCustom.XMLComment
		  
		  output.Value = Data
		  
		  Return output
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function CreateElement(TagName as Text) As FSCustom.XMLElement
		  Dim output As New FSCustom.XMLElement
		  
		  output.Name = TagName
		  
		  Return output
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function CreateTextNode(Data as Text) As FSCustom.XMLTextNode
		  Dim output As New FSCustom.XMLTextNode
		  
		  output.Value = Data
		  
		  Return output
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function ImportNode(foreignNode as FSCustom.XMLNode, optional deep as Boolean) As FSCustom.XMLNode
		  Dim newNode As FSCustom.XMLNode = Super.ImportNode( foreignNode, Self, deep )
		  Return newNode
		End Function
	#tag EndMethod


	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  Return FSCustom.XMLElement( mOwnerDocument.FirstChild )
			End Get
		#tag EndGetter
		DocumentElement As FSCustom.XMLElement
	#tag EndComputedProperty


	#tag ViewBehavior
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			InitialValue=""
			Type="String"
			EditorType=""
		#tag EndViewProperty
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
			EditorType=""
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			InitialValue=""
			Type="String"
			EditorType=""
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
			EditorType=""
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
			EditorType=""
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
