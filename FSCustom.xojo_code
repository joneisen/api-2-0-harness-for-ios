#tag Module
Protected Module FSCustom
	#tag Method, Flags = &h21
		Private Sub CleanSmartQuotes(extends  byref TheText as Text)
		  // double quotes
		  TheText = TheText.ReplaceAll( Text.FromUnicodeCodepoint(8220), Text.FromUnicodeCodepoint(34))
		  TheText = TheText.ReplaceAll( Text.FromUnicodeCodepoint(8221), Text.FromUnicodeCodepoint(34))
		  // single quotes
		  TheText = TheText.ReplaceAll( Text.FromUnicodeCodepoint(8216), Text.FromUnicodeCodepoint(39))
		  TheText = TheText.ReplaceAll( Text.FromUnicodeCodepoint(8217), Text.FromUnicodeCodepoint(39))
		  // lower right quotes
		  TheText = TheText.ReplaceAll( Text.FromUnicodeCodepoint(8218), Text.FromUnicodeCodepoint(39)) // ‚
		  TheText = TheText.ReplaceAll( Text.FromUnicodeCodepoint(8222), Text.FromUnicodeCodepoint(34)) // „
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function CloneDict(extends dict as Xojo.Core.Dictionary) As Xojo.Core.Dictionary
		  If dict <> Nil Then
		    Dim TDict As New Xojo.Core.Dictionary()
		    For Each e As Xojo.Core.DictionaryEntry In dict
		      TDict.Value(e.Key) = e.Value
		    Next
		    Return TDict
		  End If
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function isUnicodeSpace(extends char as Integer) As Boolean
		  Select Case char
		  Case 32, 160, 5760, 8192, 8193, 8194, 8195, 8196, 8197, 8198, 8199, 8200, 8201, 8202, 8239, 8287, 12288
		    Return True
		  End Select
		  Return False
		End Function
	#tag EndMethod


	#tag ViewBehavior
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			InitialValue=""
			Type="String"
			EditorType=""
		#tag EndViewProperty
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
			EditorType=""
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			InitialValue=""
			Type="String"
			EditorType=""
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
			EditorType=""
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
			EditorType=""
		#tag EndViewProperty
	#tag EndViewBehavior
End Module
#tag EndModule
