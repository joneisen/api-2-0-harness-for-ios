#tag Module
Protected Module FSAPI
	#tag Method, Flags = &h0
		Sub TextValue(extends  ByRef o as FSAPI.Operator, assigns value as Text)
		  Select Case value.Lowercase
		  Case "equals", "eq", "equal", "="
		    o = FSAPI.Operator.Equals
		  Case "contains", "cn", "contain", "*word*"
		    o = FSAPI.Operator.Contains
		  Case "beginswith", "bw", "beginwith", "word*"
		    o = FSAPI.Operator.BeginsWith
		  Case "endswith", "ew", "endwith", "*word"
		    o = FSAPI.Operator.EndsWith
		  Case "greaterthan", "gt", ">"
		    o = FSAPI.Operator.GreaterThan
		  Case "greaterthanorequal", "gte", "≥", ">=", "greaterthanequal", "greaterthanorequalto", "greaterthanequalto"
		    o = FSAPI.Operator.GreaterThanOrEqualTo
		  Case "lessthan", "lt", "<"
		    o = FSAPI.Operator.LessThan
		  Case "lessthanorequal", "lte", "≤", "<=", "lessthanequal", "lessthanorequalto", "lessthanequalto"
		    o = FSAPI.Operator.LessThanOrEqualTo
		  Case "omit", "neq", "≠", "notequal"
		    o = FSAPI.Operator.NotEqual
		  Else
		    Break
		    o = FSAPI.Operator.Equals
		  End Select
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub TextValue(extends  byref s as FSAPI.Sector, assigns value as Text)
		  Select Case value.Lowercase
		  Case "courses"
		    s = FSAPI.Sector.Courses
		  Case "crm"
		    s = FSAPI.Sector.CRM
		  Case "retail"
		    s = FSAPI.Sector.Retail
		  End Select
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function ToBoolean(extends s as Text) As Boolean
		  Select Case s.Lowercase
		  Case "true", "t", "1", "yes"
		    Return True
		  Else
		    Return False
		  End Select
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function ToOperator(extends TextValue as Text) As Operator
		  Select Case TextValue
		  Case "equals", "eq", "equal", "="
		    Return FSAPI.Operator.Equals
		  Case "contains", "cn", "contain", "*word*"
		    Return FSAPI.Operator.Contains
		  Case "beginswith", "bw", "beginwith", "word*"
		    Return FSAPI.Operator.BeginsWith
		  Case "endswith", "ew", "endwith", "*word"
		    Return FSAPI.Operator.EndsWith
		  Case "greaterthan", "gt", ">"
		    Return FSAPI.Operator.GreaterThan
		  Case "greaterthanorequal", "gte", "≥", ">=", "greaterthanequal", "greaterthanorequalto", "greaterthanequalto"
		    Return FSAPI.Operator.GreaterThanOrEqualTo
		  Case "lessthan", "lt", "<"
		    Return FSAPI.Operator.LessThan
		  Case "lessthanorequal", "lte", "≤", "<=", "lessthanequal", "lessthanorequalto", "lessthanequalto"
		    Return FSAPI.Operator.LessThanOrEqualTo
		  Case "omit", "neq", "≠", "notequal"
		    Return FSAPI.Operator.NotEqual
		  Else
		    Raise New UnsupportedOperationException
		  end select
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function ToText(extends b as Boolean) As Text
		  If b Then
		    Return "True"
		  Else
		    Return "False"
		  End If
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function ToText(extends enumValue as FSAPI.Operator) As Text
		  Select Case enumValue
		  case Operator.Equals
		    return "Equals"
		  case Operator.Contains
		    return "Contains"
		  case Operator.BeginsWith
		    return "BeginsWith"
		  case Operator.EndsWith
		    return "EndsWith"
		  case Operator.GreaterThan
		    return "GreaterThan"
		  case Operator.GreaterThanOrEqualTo
		    return "GreaterThanOrEqualTo"
		  case Operator.LessThan
		    return "LessThan"
		  case Operator.LessThanOrEqualTo
		    return "LessThanOrEqualTo"
		  case Operator.NotEqual
		    return "NotEqual"
		  Else
		    raise new UnsupportedOperationException
		  end select
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function ToText(extends s as FSAPI.Sector) As Text
		  Select Case s
		  Case FSAPI.Sector.Courses
		    Return "Courses"
		  Case FSAPI.Sector.CRM
		    Return "CRM"
		  Case FSAPI.Sector.Retail
		    Return "Retail"
		  Else
		    //fuck
		    Break
		  End Select
		End Function
	#tag EndMethod


	#tag Enum, Name = Operator, Type = Integer, Flags = &h0
		Equals
		  Contains
		  BeginsWith
		  EndsWith
		  GreaterThan
		  GreaterThanOrEqualTo
		  LessThan
		  LessThanOrEqualTo
		NotEqual
	#tag EndEnum

	#tag Enum, Name = Sector, Type = Integer, Flags = &h0
		CRM
		  Retail
		Courses
	#tag EndEnum


	#tag ViewBehavior
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			InitialValue=""
			Type="String"
			EditorType=""
		#tag EndViewProperty
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
			EditorType=""
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			InitialValue=""
			Type="String"
			EditorType=""
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
			EditorType=""
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
			EditorType=""
		#tag EndViewProperty
	#tag EndViewBehavior
End Module
#tag EndModule
